<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">How to Buy</h3>
    </div>
    <div class="panel-body">
      <div class="alert alert-info" role="alert">Cash only. We do not have credit card facilities</div>
      <ol>
        <li style="font-weight: bold;">Always Call/Text/Message first to Check for Availability</li>
            <p>Centralized Hotline for both stores</p>
            <ul>
              <li>572-5858</li>
              <li>232-2320</li>
              <li>0922 331 3328 Sun All Networks Text and Call</li>
              <li>0915-228-6108 Globe incoming calls only and Viber Calls and Messages 9am to 6pm</li>
              <li>FaceBook ID: cmkcellphones</li>
            </ul>
        <li style="font-weight: bold;">Stocks on Hand</li>
          <ol type='a'>
            <li>Reserve item by leaving your Text number and Full name</li>
            <li>Then choose:</li>
            <ul>
              <li>Pick Up at Quezon City (choose from the 2 locations below)</li>
                <p>Reservations valid for 1 day only<br />
                For Saturday pick up please reserve on Friday<br />
                Location Map
                <ul>
                  <li>CMK Cellphones - Roosevelt Ave. Mondays to Fridays</li>
                  <li>Quality Cellular Phone Center - West Ave Mondays to Saturdays</li>
                </ul>
              <li>Shipping thru LBC Next Day Delivery:</li>
              <ul>
                <li>For Items that are on Hand</li>
                <li>Item will arrive the next day via LBC</li>
                <li>Deposit Cash in full by 2pm</li>
                <li>All shipping of Cellphones Pls Add 200 shipping fee and 1% Insurance</li>
                <li>Shipping of Laptops Add 500 shipping fee and 1% shipping fee</li>
                <li>Some areas add 1more day for delivery</li>
                  <p>Maguindanao,
                  Ilocos Sur,
                  Surigao Del Sur,
                  Baler Aurora</p>
                <li>Some Areas 3days</li>
                  <p>Virac,
                  Sultan Kuldarat,
                  Samar, Laog Ilocos Norte,
                  Masbate,
                  and Cotabato, Zamboanga City</p>
              </ul>
            </li>
          </ol>
          <li style="font-weight: bold;">By Order Items, depending on item</li>
            <ol type='a'>
              <li>Some items can be ordered by just leaving your</li>
              <p>Full Details ( Full Name, Landline, Cellphone Number)<br />
              For Saturday pick up please order on
              Thursday to give us time to order item.
              Most suppliers are closed on Saturdays</p>
              <li>Other items must be ordered by Depositing 10% or 1,000 whichever is higher</li>
              <p>Cut off time varies depending on the Item<br />
              Items with Deposit will be reserved for 1 week Latest 2 weeks beyond this<br />
              Deposit will be forfeited and item will have to be sold to someone else<br />
              Deposits can be made at the store or the following :</p>
              <ul>
                <li>BPI</li>
                <li>Metrobank</li>
                <li>BDO</li>
                <li>Globe GCash (Villarica Pawnshops, Globe centers)</li>
                <li>Smart Money</li>
                <li>Bdo Bpi or Metrobank REmit via (LBC, Cebuana, Palawan Express ETC.)</li>
              </ul>
              <li>We will text you when the item is ready. Depending on Item and Brand Pick up time as follows</li>
              <ul>
                <li>Depending on Availability: Deposit Morning Item can be prepared by 2pm or 4pm</li>
                <li>Depending on Brand and Item : Deposit before 4pm cutoff or 5pm cutoff . Next day Item can be prepared by 2pm or 4pm</li>
                <li>Please call or text for more details on the item you are ordering</li>
              </ul>
              <li>Shipping 2days Delivery for items that have to be ordered</li>
              <ul>
                <li>Pay balance in Cash by 2pm via:</li>
                <ul>
                  <li>BPI</li>
                  <li>BDO</li>
                  <li>Metrobank</li>
                  <li>Smart Money</li>
                  <li>Globe GCash</li>
                  <li>Bdo REmit via (LBC, Cebuana, Palawan Express ETC.)</li>
                  <li>Bance De Oro Remittance via Xoom Payments and the like, if you are abroad buying for relatives in the Philippines</li>
                <li>Item will arrive via LBC ( Some areas add 1more day for delivery Maguindanao, Surigao Del Sur, Virac, Sultan Kuldarat, Samar,Cotabato, Zamboanga City, Laog Ilocos Norte; Baler Aurora )</li>
              </ul>
            </ol>
      </ol>
    </div>
  </div>
</div>
