<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">CMK Cellphones</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li <?php if ( $this->uri->uri_string() == 'home' ||  $this->uri->uri_string() == '') echo "class='active'"; ?>><a href="<?php echo site_url('home'); ?>">Home</a></li>
        <li class="dropdown <?php $tmp = $this->uri->uri_string(); if ( $tmp == 'all-products' || $tmp == 'latest-models' || $tmp == 'best-sellers' || $tmp == 'sale-promo') echo "active"; ?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Shop <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li <?php if ( $this->uri->uri_string() == 'all-products' ) echo "class='active'"; ?>><a href="<?php echo site_url('all-products'); ?>">All Products</a></li>
            <li role="separator" class="divider"></li>
            <li <?php if ( $this->uri->uri_string() == 'latest-models' ) echo "class='active'"; ?>><a href="<?php echo site_url('latest-models'); ?>">Latest Models</a></li>
            <li <?php if ( $this->uri->uri_string() == 'best-sellers' ) echo "class='active'"; ?>><a href="<?php echo site_url('best-sellers'); ?>">Best Sellers</a></li>
            <li <?php if ( $this->uri->uri_string() == 'sale-promo' ) echo "class='active'"; ?>><a href="<?php echo site_url('sale-promo'); ?>">Sale Items / Promos</a></li>
          </ul>
        </li>
        <li <?php if ( $this->uri->uri_string() == 'how-to-buy' ) echo "class='active'"; ?>><a href="<?php echo site_url('how-to-buy'); ?>">How to Buy</a></li>
        <li <?php if ( $this->uri->uri_string() == 'location-map' ) echo "class='active'"; ?>><a href="<?php echo site_url('location-map'); ?>">Location Map</a></li>
        <li <?php if ( $this->uri->uri_string() == 'faqs' ) echo "class='active'"; ?>><a href="<?php echo site_url('faqs'); ?>">FAQS</a></li>
        <li <?php if ( $this->uri->uri_string() == 'policies' ) echo "class='active'"; ?>><a href="<?php echo site_url('policies'); ?>">Policies</a></li>
        <li <?php if ( $this->uri->uri_string() == 'contact-us' ) echo "class='active'"; ?>><a href="<?php echo site_url('contact-us'); ?>">Contact Us</a></li>
      </ul>
        <form class="navbar-form navbar-right" role="search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search products">
            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </div>
          </div>
        </form>
    </div><!--/.nav-collapse -->
  </div>
</nav>

<div class="container">
