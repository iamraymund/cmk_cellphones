<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class How_to_buy extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('how_to_buy');
		$this->load->view('footer');
		$this->load->view('footer_cont');

	}
}
