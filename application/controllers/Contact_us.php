<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('contact_us');
		$this->load->view('footer');
		$this->load->view('contact_us_scripts');
		$this->load->view('footer_cont');

	}
}
