<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Latest_models extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('latest_models');
		$this->load->view('footer');
		$this->load->view('footer_cont');

	}
}
