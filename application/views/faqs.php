<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Frequently Asked Question</h3>
    </div>
    <div class="panel-body">
      <ol class="panel-list-spacing bold">
        <li>Where can we pick up the items?</li>
          <p>2 shop locations:
          CMK Cellphones - 311 B Roosevelt Ave., QC (600 meters from LRT1 roosevelt station) mon-fri 9am-6pm
          Quality Cellular 144-C West Ave. (near EDSA, use MRT North Station) Mon-Sat 9am-6pm</p>
        <li>What does it mean that your units are NTC registered</li>
          <p>Units that are NTC registered have the official 1 year warranty of parts and service covered by the manufacturer</p>
        <li>Are u open on Sundays?</li>
          <p>No, we are open mondays to friday 9am to 6pm (cmk roosevelt branch) and monday to saturday (quality west ave branch) 9am to 6pm , Open on some holidays til 4pm</p>
        <li>Can we go straight to the service center to claim the warranty?</li>
          <p>Yes, present the receipt we gave you upon purchase to claim warranty</p>
        <li>How can I be sure I will receive the item if I decide to have it shipped and I pay through the bank?</li>
          <p>We are a company selling online, not reseller, so our bank account is under cmk cellphone
          you can be sure since we are protecting a company name we will surely send the item you have paid for</p>
        <li>Does warranty include parts or just service?</li>
          <p>Since all our units are covered by the manufacturer, yes warranty covers both parts and service as long as it is a defect
          and not caused by misuse. Example broken lcd or wet units not covered. Physical issues also not covered since this is wear and tear and not a defect</p>
        <li>When can i expect lower prices?</li>
          <p>Our website is updated everyday if there are any changes in price</p>
        <li>If i dont see the model on your site can we order it ?</li>
          <p>If it is a new model this is possible once it gets officially released. If its an old model its probably been removed from the site because it has reached its end of life or has been phased out.</p>
        <li>Can we go straight to your shop and buy your item?</li>
          <p>It is best if u call or text for availability before you come to the shop. Some items are by order</p>
        <li>Do u accept credit card for straight/installment?</li>
          <p>we stopped accepting credit cards so that our prices can be lower, no add on charges</p>
        <li>I am Located outside metro manila i cannot pick up at your store, how can i buy?</li>
          <p>We can Ship via LBC.
          Shipping 1 to 2days Delivery depending on item
          Pay in Full by 12noon via:
          BPI,
          BDO,
          Metrobank,
          Globe GCash (Villarica Pawnshops, Globe centers),
          Smart Money,
          Bdo Bpi or Metrobank REmit via (LBC, Cebuana, Palawan Express ETC.)
          Item will arrive via LBC mindanao areas pls expect additional day delivery, Surigao Del Sur, Virac</p>
        <li>Do all your items need deposit first to order? Can't we just walk in to buy?</li>
          <p>It depends on the item, some items are readily available and dont need deposit just reservation on the day of pick up. Items not on stock need deposit to order since its a special order. The deposit serves as a guarantee that you will pick up the item you ordered.</p>
        <li>Why do i need to reserve the item before i pick it up, cant i just go straight to your store?</li>
          <p>The reservation is actually for your convenience. Some items are limited and on a given day several buyers may buy the item. The reservation ensures that when you come to the store the item has been reserved for you and will not be given to someone else even if they arrived earlier. Reservations are valid for one day only.</p>
      </ol>
    </div>
  </div>
</div>
