<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location_map extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('location_map');
		$this->load->view('footer');
		$this->load->view('location_map_scripts');
		$this->load->view('footer_cont');

	}
}
