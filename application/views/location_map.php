<div class="col-lg-12">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Location Map</h3>
    </div>
    <div class="panel-body">
      <div>
        <a href="#">
          <img class="thumbnail img-responsive" src='<?php echo base_url('mrt-lrt.gif'); ?>'/>
        </a>
      </div>
      <h3>CMK Cellphones</h3>
      <p>MCCM Building<br />
        311-B Roosevelt Avenue, Quezon City<br />
        From Edsa before corner of MH Del Pilar(UCPB)<br />
        Half a kilometer from Edsa Munoz<br />
        Right side of Roosevelt Ave after LBC, Across BPI Family, Beside Robinsons Bank I. Take LRT 1<br />
      </p>
      <ol type="I">

        <li>From as far as Baclaran to Roosevelt Station (EDSA corner Roosevelt Ave) only 500 meters from cmk cellphones</li>
        <ul>
          <li>From EDSA corner Roosevelt Avenue (Munoz Market): </li>
          <li>you will see a 7-eleven (right side),pass LBC on the right,</li>
          <li>Pass Barangay City Hall (left side), then Bpi Family (left side)</li>
          <li>CMK Cellphones is located on the right side, beside Robinson'bank.</li>
        </ul>

        <li>OR from North Ave. MRT Station:</li>
          <ul>
            <li>Ride any bus going to Monumento.</li>
            <li>Go down at McDonalds, then cross the footbridge to reach Roosevelt Avenue.</li>
            <li>From EDSA corner Roosevelt Avenue (Munoz Market):</li>
            <li>you will see a 7-eleven (right side),pass LBC on the right,</li>
            <li>Pass Barangay City Hall (left side), then Bpi Family (left side)</li>
            <li>CMK Cellphones is located on the right side, beside Robinson's Bank.</li>
          </ul>

          <li>Or from Quiapo/Quezon Ave Ride Dyip to munoz/proj8:</li>
            <ul>
              <li>pass intersection of Del Monte (Petron), pass Bayantel, pass Baler (Shell Gas Station) and Coca Cola Plant.</li>
              <li>then you will see Chinabank and Metrobank on your LEFT,</li>
              <li>the next intersection is M.H. Del Pilar St.(UCPB bank), pass this corner.</li>
              <li>you will see CMK CELLPHONES on the LEFT, between Robinson's Bank and Topwood Hardware Store.</li>
            </ul>
      </ol>
   <div>
     <a href="#">
       <img class="thumbnail img-responsive" src='<?php echo base_url('quality-cellular-map.jpg'); ?>'/>
     </a>
   </div>

    <h3>Quality Cellular Store Hours</h3>
    <p>144-C West Avenue, Quezon City<br />
      From Edsa Paramount 300 meters along west ave<br />
      After Caltext Yokohama Kidney Dialysis Center and<br />
      Across Landbank
    </p>
    <div>
      <a href="#">
        <img class="thumbnail img-responsive" src='<?php echo base_url('cmk-cellphone-map.jpg'); ?>'/>
      </a>
    </div>s
    </div>
  </div>
</div>

<!-- Modal Image -->
<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-image">
  		    <button class="close" type="button" data-dismiss="modal">×</button>
  	  </div>
    	<div class="modal-body">
    	</div>
    </div>
  </div>
</div>
<!-- /Modal Image -->
