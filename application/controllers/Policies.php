<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Policies extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('policies');
		$this->load->view('footer');
		$this->load->view('footer_cont');

	}
}
