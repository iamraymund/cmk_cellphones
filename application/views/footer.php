</div> <!-- /container -->

<footer class="footer">
  <div class="border"></div>
  <div class="container">
    <div class="row col-md-12">
      <div class="col-md-3">
        <h4 class="footer-title">CMK Cellphones Store<hr ></h4>
        <p class="footer-subtitle">Store Address: </p>
        <p class="footer-subtitle-content">MCCM Building<br />
          311-B Roosevelt Avenue, Quezon City<br />
          From Edsa before corner of MH Del Pilar(UCPB)<br />
          Half a kilometer from Edsa Munoz<br />
          Right side of Roosevelt Ave after LBC, Across BPI Family, Beside Robinsons Bank
        </p>
      </div>
      <div class="col-md-3">
        <h4 class="footer-title">Quality Cellular Store<hr ></h4>
        <p class="footer-subtitle">Store Address: </p>
        <p class="footer-subtitle-content">Quality Cellular Phone Center<br />
          144-C West Avenue, Quezon City<br />
          From Edsa Paramount 300 meters along west ave<br />
          Across Landbank Beside Labandera Queen</p>
      </div>
      <div class="col-md-3">
        <h4 class="footer-title">Contact Us<hr ></h4>

        <p class="footer-subtitle">Store Hours: </p>
        <p class="footer-subtitle-content">Mon-Sat: 9AM-6PM</p>
        <p class="footer-subtitle">Call / Text: </p>
        <p class="footer-subtitle-content">
          (02)572-5858 or 232-2320<br />
          (0922)331-3328 Hotline and Viber<br />
          For Globe unli users,  please call our globe ( incoming calls only )<br />
          0915-228-6108
        </p>
      <p class="footer-subtitle">Email Address: </p>
      <p class="footer-subtitle-content"><a href="mailto:cmkcellphones@yahoo.com">cmkcellphones@yahoo.com</a></p>
      </div>
      <div class="col-md-3">
        <h4 class="footer-title">Let's Connect<hr ></h4><br />
        <ul class="social-network social-circle">

            <li><a href="https://www.facebook.com/cmkcellphones/" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/cmkcellphones" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <!--li><a href="#" class="icoRss" title="Rss"><i class="fa fa-rss"></i></a></li>
            <li><a href="#" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li-->
        </ul>
          <p><br />All Rights Reserved. Copyright <?php echo date("Y"); ?><br />Developed by Raymund Edgar Alvarez</p>
      </div>
    </div>

  </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url('assets/jquery-3.1.1/jquery.min.js'); ?>" ></script>

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url('assets/bootstrap-3.3.7/dist/js/bootstrap.min.js'); ?>"></script>

<!-- Bootstrap Table JS -->
<!-- <script src="<?php echo base_url('assets/bootstrap-table-1.11.0/dist/bootstrap-table.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table-1.11.0/dist/extensions/export/bootstrap-table-export.js'); ?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table-1.11.0/dist/extensions/export/tableExport/tableExport.min.js'); ?>"></script> -->

<!-- Bootstrap Toast -->
<!-- <script src="<?php echo base_url('assets/toastr-2.1.3/toastr.min.js'); ?>"></script> -->

<!-- Selectize JS -->
<!-- <script type="text/javascript" src="<?php echo base_url('assets/selectize/dist/js/standalone/selectize.js'); ?>"></script> -->
