<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All_products extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('all_products');
		$this->load->view('footer');
		$this->load->view('footer_cont');
	}
}
