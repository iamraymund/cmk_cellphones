<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller {

	public function index()
	{
		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('faqs');
		$this->load->view('footer');
		$this->load->view('footer_cont');

	}
}
