<div class="col-lg-12">
  <div class="col-lg-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">CMK Cellphones Store</h3>
      </div>
      <div class="panel-body">
        <p class="no-margin-title">Store Address: </p>
        <p class="footer-subtitle-content">MCCM Building<br />
          311-B Roosevelt Avenue, Quezon City<br />
          From Edsa before corner of MH Del Pilar(UCPB)<br />
          Half a kilometer from Edsa Munoz<br />
          Right side of Roosevelt Ave after LBC, Across BPI Family, Beside Robinsons Bank
        </p>
        <div>
          <a href="#">
            <img class="thumbnail img-responsive" src='<?php echo base_url('quality-cellular-map.jpg'); ?>'/>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Quality Cellular Store</h3>
      </div>
      <div class="panel-body">
        <p class="no-margin-title">Store Address: </p>
        <p class="footer-subtitle-content">Quality Cellular Phone Center<br />
          144-C West Avenue, Quezon City<br />
          From Edsa Paramount 300 meters along west ave<br />
          Across Landbank Beside Labandera Queen</p>
          <div>
            <a href="#">
              <img class="thumbnail img-responsive" src='<?php echo base_url('cmk-cellphone-map.jpg'); ?>'/>
            </a>
          </div>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Contact Us</h3>
      </div>
      <div class="panel-body">
        <p class="no-margin-title">Store Hours: </p>
        <p class="footer-subtitle-content">Mon-Sat: 9AM-6PM</p>
        <p class="footer-subtitle">Call / Text: </p>
        <p class="footer-subtitle-content">
          (02)572-5858 or 232-2320<br />
          (0922)331-3328 Hotline and Viber<br />
          For Globe unli users,  please call our globe ( incoming calls only )<br />
          0915-228-6108
        </p>
        <p class="footer-subtitle">Email Address: </p>
        <p class="footer-subtitle-content"><a href="mailto:cmkcellphones@yahoo.com">cmkcellphones@yahoo.com</a></p>

      </div>
    </div>
  </div>
</div>
</div>

<!-- Modal Image -->
<div tabindex="-1" class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-image">
  		    <button class="close" type="button" data-dismiss="modal">×</button>
  	  </div>
    	<div class="modal-body">
    	</div>
    </div>
  </div>
</div>
<!-- /Modal Image -->
