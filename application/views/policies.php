<div class="col-lg-12">
  <div class="col-lg-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Ordering Policies</h3>
      </div>
      <div class="panel-body">
        <ul class="panel-list-spacing">
          <li>Deposits for special order items will be forfeited in our favor for orders not picked up or for any cancellation of orders</li>
          <li>LBC Third Party Courier Service.
              Mandatory Insurance to cover loss for items Shipped.
              Cost: 1% of declared value of item, Charged to buyer.
          </li>
          <li>It is the responsibilty of the customer to make sure the product they are purchasing
          has the features, functionality and connectivity required
          if in doubt please contact the manufacturer.</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Warranty Policies</h3>
      </div>
      <div class="panel-body">
        <ul class="panel-list-spacing">
          <li>Asus and Apple No longer Cover Software as Factory Defect if the problem is software there will be charges depending on the service center</li>

          <li>24hour Replacement Policy in case of defects only</li>

          <li>Units purchased have 1 year
          warranty at authorized service centers.</li>

          <li>We may receive your unit for
          facilitation of your repairs only.</li>

          <li>We are not responsible for the
          warranty of your unit.</li>

          <li>Only the manufacturer covers the warranty.</li>

          <li>Your receipt is your proof of warranty.
          please keep your receipt for warranty purposes.
          no receipt, no warranty.</li>

          <li>Warranty covers factory defects only,
          not wear and tear or negligence like dropped or wet unit</li>
        </ul>
      </div>
    </div>
  </div>
</div>
